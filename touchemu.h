#ifndef _TOUCHEMU_H_
#define _TOUCHEMU_H_

#define TOUCH_PATH "/dev/input/touchscreen"

#include "preload.h"

#pragma GCC visibility push(hidden)

extern int touch_open(const char *pathname, int flags, mode_t mode, FileOps* ops);

#pragma GCC visibility pop

#endif
