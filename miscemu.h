#ifndef MISCEMU_H
#define MISCEMU_H

#define NDUID_PATH "/proc/nduid"

#include "preload.h"

#pragma GCC visibility push(hidden)

extern int nduid_open(const char *pathname, int flags, mode_t mode, FileOps* ops);

#pragma GCC visibility pop

#endif
