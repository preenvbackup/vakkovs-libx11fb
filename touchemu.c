#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <linux/input.h>

#include "config.h"
#include "preload.h"
#include "touchemu.h"
#include "client.h"
#include "log.h"

static int servfd = -1;

static int touch_close(int fd);
static int touch_ioctl(int fd, unsigned long request, void * argp);
static ssize_t touch_read(int fd, void *buf, size_t count);

static int button_pressed;

int touch_open(const char *pathname, int flags, mode_t mode, FileOps* ops)
{
	if (servfd == -1) {
		servfd = client_connect();
		if (servfd == -1) {
			WARNING("Unable to connect to x11fbd");
			return -1;
		}
	}

	int fd = real_open("/dev/zero", O_RDONLY, 0666);
	if (fd == -1) {
		WARNING("Unable to open /dev/zero");
		return -1;
	}

	TRACE("Opened touch device %s (fake fd %d)", pathname, fd);

	ops->close = touch_close;
	ops->ioctl = touch_ioctl;
	ops->read = touch_read;

	button_pressed = 0;

	return fd;
}

static int touch_close(int fd)
{
	return real_close(fd);
}

static int touch_ioctl(int fd, unsigned long request, void * argp)
{
	LOG("Touch got an ioctl! 0x%lx", request);
	return 0;
}

static struct timeval temp_tp;

static ssize_t touch_read(int fd, void *buf, size_t count)
{
	XFPPacket pkt;

	pkt.type = XFP_TYPE_WAIT_MOTION;
	send_packet(servfd, &pkt);

	if (CHECKED_RECV_PACKET(servfd, &pkt, XFP_TYPE_WAIT_MOTION)) { /* Blocks */
		struct timeval tp;
		struct input_event e[6];
		size_t len;
		int i = 0;

		gettimeofday(&tp, NULL);
		gettimeofday(&temp_tp, NULL);

#if PRE_QUIRKS
		e[i].time = tp;
		e[i].type = 7;
		e[i].code = 0;
		e[i].value = (unsigned) &temp_tp;
		i++;
#endif

	if (pkt.data.motion.gesture) {
		e[i].time = tp;
		e[i].type = 6;
		e[i].code = pkt.data.motion.gesture;
		e[i].value = pkt.data.motion.pressed;
		i++;
	} else {
		if (pkt.data.motion.pressed != button_pressed) {
			/* State of button changed */
			e[i].time = tp;
			e[i].type = EV_KEY;
			e[i].code = BTN_TOUCH;
			e[i].value = pkt.data.motion.pressed;
			button_pressed = pkt.data.motion.pressed;
			i++;
		}

		e[i].time = tp;
		e[i].type = EV_ABS;
		e[i].code = ABS_X;
		e[i].value = pkt.data.motion.x;
		i++;

		e[i].time = tp;
		e[i].type = EV_ABS;
		e[i].code = ABS_Y;
		e[i].value = pkt.data.motion.y;
		i++;
	}

		e[i].time = tp;
		e[i].type = EV_SYN;
		e[i].code = SYN_REPORT;
		e[i].value = 0;
		i++;

		len = sizeof(struct input_event) * i;
		if (count < len) len = count; /* Truncate */
		memcpy(buf, e, len);
		return len;
	}

	return -1;
}

