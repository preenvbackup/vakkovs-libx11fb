#ifndef CONFIG_H
#define CONFIG_H

#define PRE_QUIRKS 1
#define N900_HAA_SCALE 1

#define DEFAULT_W 800
#define DEFAULT_H 480
#define DEFAULT_DEPTH 32

#define DEFAULT_SHM_SIZE (1125 * 4096)
	/* 1125 pages is 800x480x32 x 3 planes, more than enough. */

#define MAX_QUEUED_EVENTS 16

#endif
