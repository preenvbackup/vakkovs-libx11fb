#include <stdio.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/extensions/XShm.h>
#include <linux/input.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <glib.h>

#include "config.h"
#include "x11.h"
#include "fbserv.h"
#include "protocol.h"

#include "atoms.inc"

int shm_size = DEFAULT_SHM_SIZE;
int win_w = DEFAULT_W, win_h = DEFAULT_H;
int win_depth = DEFAULT_DEPTH;

static Display* display;
static GIOChannel *chan;
static guint watch;

static int screen;
static Window root, parent, window;
static XVisualInfo vinfo;
static Colormap colormap;
static XImage *image;
static GC gc;
static XShmSegmentInfo shminfo;

static int offset_x, offset_y;

#if N900_HAA_SCALE
static void actor_send_message(Atom message_type,
	long l0, long l1, long l2, long l3, long l4)
{
	XEvent event = { 0 };

	event.xclient.type = ClientMessage;
	event.xclient.window = window;
	event.xclient.message_type = message_type;
	event.xclient.format = 32;
	event.xclient.data.l[0] = l0;
	event.xclient.data.l[1] = l1;
	event.xclient.data.l[2] = l2;
	event.xclient.data.l[3] = l3;
	event.xclient.data.l[4] = l4;

	XSendEvent(display, window, True,
		StructureNotifyMask,
		(XEvent *)&event);
}

static void actor_ready()
{
	int status;
	Atom actual_type;
	int actual_format;
	unsigned long nitems, bytes_after;
	unsigned char *prop = NULL;

	status = XGetWindowProperty(display, window,
		ATOM(_HILDON_ANIMATION_CLIENT_READY), 0, 32,
		False, XA_ATOM,
		&actual_type, &actual_format,
		&nitems, &bytes_after, &prop);

	if (prop) {
		XFree(prop);
	}

	if (status != Success || actual_type != XA_ATOM ||
			actual_format != 32 || nitems != 1)  {
		return;
	}
g_debug("Configuring actor");
	actor_send_message(ATOM(_HILDON_ANIMATION_CLIENT_MESSAGE_PARENT),
		parent, 0, 0, 0, 0);
	actor_send_message(ATOM(_HILDON_ANIMATION_CLIENT_MESSAGE_POSITION),
		0, 480, 0, 0, 0);
	actor_send_message(ATOM(_HILDON_ANIMATION_CLIENT_MESSAGE_SCALE),
		1.5 * (1 << 16), 1.5 * (1 << 16), 0, 0, 0);
	actor_send_message(ATOM(_HILDON_ANIMATION_CLIENT_MESSAGE_ROTATION),
		2, -90.0 * (1 << 16), 0, 0, 0);
	actor_send_message(ATOM(_HILDON_ANIMATION_CLIENT_MESSAGE_SHOW),
		1, 255, 0, 0, 0);
}

static inline void scale(int *x, int *y)
{
	int old_x = *x, old_y = *y;
	*x = 320 - ( (320 * old_y) / 480 );
	*y = (old_x * 480) / 720;
}

static inline int in_range(int i, int a, int b)
{
	return i >= a && i < b;
}

static void fake_key_insert(int keycode)
{
	g_debug("fake keycode %d", keycode);
	fbserv_notify_key(keycode, 1);
	fbserv_notify_key(keycode, 0);
}

static int gesture_start_x, gesture_start_y;
#endif

static inline void notify_motion(int x, int y, int state, int change)
{
#if N900_HAA_SCALE
	if (change > 0) {
		gesture_start_x = x;
		gesture_start_y = y;
	} else if (change < 0) {
		/* Think Different (in this case: horizontal) */
		if (gesture_start_x >= 720 && x < 680) {
			g_debug("Minimize card gesture");
			fbserv_notify_gesture(3);
		} else if (gesture_start_x >= 720 && x >= 720 &&
			gesture_start_y >= 240 && y < 240) {
			g_debug("Forward gesture");
			fbserv_notify_gesture(4);
		} else if (gesture_start_x >= 720 && x >= 720 &&
			gesture_start_y < 240 && y >= 240) {
			g_debug("Back gesture");
			fbserv_notify_gesture(5);
		} else if (gesture_start_x >= 720 && x >= 720 &&
			in_range(gesture_start_y, 200, 280) && in_range(y, 200, 280)) {
			g_debug("Center gesture");
			fake_key_insert(/*KEY_CENTER*/232);
		}
	}

	scale(&x, &y);
#endif

	if (change > 0) state = 1;
	else if (change < 0) state = 0;

	fbserv_notify_motion(x, y, state);
}

static gboolean x11_watch_cb(GIOChannel *source, GIOCondition condition,
	gpointer data)
{
	XEvent e;
	while (XPending(display)) {
		XNextEvent(display, &e);
		switch (e.type) {
			case KeyPress:
				fbserv_notify_key(e.xkey.keycode - 8, 1);
			break;
			case KeyRelease:
				fbserv_notify_key(e.xkey.keycode - 8, 0);
			break;
			case MotionNotify:
				notify_motion(e.xmotion.x, e.xmotion.y,
					e.xmotion.state & Button1Mask ? 1 : 0, 0);
			break;
			case ButtonPress:
				notify_motion(e.xbutton.x, e.xbutton.y,
					e.xmotion.state & Button1Mask ? 1 : 0,
					e.xbutton.button == Button1 ?  1 : 0);
			break;
			case ButtonRelease:
				notify_motion(e.xbutton.x, e.xbutton.y,
					e.xmotion.state & Button1Mask ? 1 : 0,
					e.xbutton.button == Button1 ? -1 : 0);
			break;
			case PropertyNotify:
#if N900_HAA_SCALE
				if (e.xproperty.atom == ATOM(_HILDON_ANIMATION_CLIENT_READY)) {
					actor_ready();
				}
#endif
			break;
			case FocusIn:
#if N900_HAA_SCALE
				XSync(display, False);
				XUnmapWindow(display, window);
				XSync(display, False);
				XMapWindow(display, window);
				XFlush(display);
#endif
			break;
			case Expose:
				x11_draw();
			break;
		}
	}

	return TRUE;
}

static void x11_select_visual()
{
	if (!XMatchVisualInfo(display, screen, win_depth, TrueColor, &vinfo)) {
		/* Not matched; Use the default visual instead */
		int numVisuals;
		XVisualInfo *xvi;

		vinfo.screen = screen;
		xvi = XGetVisualInfo(display, VisualScreenMask, &vinfo, &numVisuals);
		g_assert(xvi);

		vinfo = *xvi;
		XFree(xvi);
	}
}

static void x11_create_image()
{
	Status status;

	if (vinfo.visual != DefaultVisual(display, screen)) {
		/* Allocate a private color map. */
		colormap = XCreateColormap(display, root, 
			vinfo.visual, AllocNone);
		g_debug("Allocating custom colormap");
	} else {
		colormap = 0;
	}

	image = XShmCreateImage(display, vinfo.visual, vinfo.depth, ZPixmap, NULL,
		&shminfo, win_w, win_h);
	g_assert(image);

	status = XShmAttach(display, &shminfo);
	g_assert(status == 1);

	image->data = shminfo.shmaddr;

	g_debug("Image created");
}

static void x11_create_window()
{
	unsigned long pid = getpid();
	XClassHint classhint = { "x11fb", "x11fb" };
	Atom atom;
	XSetWindowAttributes attr;
	unsigned long attrmask = CWBorderPixel | CWBackPixel | CWBitGravity;
	XWMHints hints;
	attr.background_pixel = 0;
	attr.border_pixel = attr.background_pixel;
	attr.bit_gravity = ForgetGravity;
	attr.cursor = None;
	if (colormap) {
		attr.colormap = colormap;
		attrmask |= CWColormap;
	}
	hints.input = True;
	hints.flags = InputHint;

#if N900_HAA_SCALE
	parent = XCreateWindow(display, root, 0, 0, 800, 480, 0, CopyFromParent,
		InputOutput, CopyFromParent, attrmask & ~CWColormap, &attr);
	window = XCreateWindow(display, root, 0, 0, win_w, win_h, 0, vinfo.depth,
		InputOutput, vinfo.visual, attrmask, &attr);

	atom = ATOM(_NET_WM_STATE_FULLSCREEN);
	XChangeProperty(display, parent, ATOM(_NET_WM_STATE),
		XA_ATOM, 32, PropModeReplace,
		(unsigned char *) &atom, 1);

	atom = ATOM(_HILDON_WM_WINDOW_TYPE_ANIMATION_ACTOR);
	XChangeProperty(display, window, ATOM(_NET_WM_WINDOW_TYPE),
		XA_ATOM, 32, PropModeReplace,
		(unsigned char *) &atom, 1);
#else
	parent = XCreateWindow(display, root, 0, 0, win_w, win_h, 0, CopyFromParent,
		InputOutput, CopyFromParent, attrmask & ~CWColormap, &attr);
	window = XCreateWindow(display, root, 0, 0, win_w, win_h, 0, vinfo.depth,
		InputOutput, vinfo.visual, attrmask, &attr);
#endif

	XStoreName(display, parent, "X11Fb");
	XSetWMProperties(display, parent, NULL, NULL, NULL, 0, NULL,
		&hints, &classhint);
	XChangeProperty(display, parent, ATOM(_NET_WM_PID), XA_CARDINAL, 32,
		PropModeReplace, (unsigned char *)&pid, 1);
	XStoreName(display, window, "X11Fb Subwindow");

	XSelectInput(display, parent,
		KeyPressMask | KeyReleaseMask | PointerMotionMask |
		ButtonPressMask | ButtonReleaseMask | FocusChangeMask | ExposureMask);
	XSelectInput(display, window, PropertyChangeMask);

	gc = XCreateGC(display, window, 0, NULL);

	XMapWindow(display, window);
	XMapWindow(display, parent);

	XFlush(display);
}

static void x11_destroy_image()
{
	XShmDetach(display, &shminfo);
	XDestroyImage(image);
	if (colormap) {
		XFreeColormap(display, colormap);
		colormap = 0;
	}
}

static void x11_destroy_window()
{
	XDestroyWindow(display, parent);
	XFreeGC(display, gc);
	XDestroyWindow(display, window);
	XFlush(display);
}

void x11_start()
{
	display = XOpenDisplay(NULL);
	g_warn_if_fail(display != NULL);
	screen = DefaultScreen(display);
	root = RootWindow(display, screen);

	XInternAtoms(display, (char**)atom_names, ATOM_COUNT, True, atom_values);

	g_debug("Allocating %d pages of shared memory", shm_size / 4096);

	shminfo.shmid = shmget(IPC_PRIVATE, shm_size, IPC_CREAT|0777);
	g_assert(shminfo.shmid >= 0);

	shminfo.shmaddr = shmat(shminfo.shmid, NULL, 0);
	g_assert(shminfo.shmaddr);

	shminfo.readOnly = False;

	shmctl(shminfo.shmid, IPC_RMID, 0);

	x11_select_visual();
	x11_create_image();
	x11_create_window();

	/* Set up X11 event monitor */
	chan = g_io_channel_unix_new(ConnectionNumber(display));
	watch = g_io_add_watch(chan, G_IO_IN | G_IO_ERR, x11_watch_cb, NULL);

	offset_x = 0;
	offset_y = 0;
}

void x11_stop()
{
	x11_destroy_image();
	x11_destroy_window();

	g_debug("X resources freed");

	shmdt(shminfo.shmaddr);
	g_source_remove(watch);
	g_io_channel_unref(chan);
	XCloseDisplay(display);
}

int x11_get_shmid()
{
	return shminfo.shmid;
}

void x11_set_offset(int x, int y)
{
	/* TODO: Understand how fbdev flipping works */
	offset_x = x;
	offset_y = y;
}

void x11_draw()
{
	XShmPutImage(display, window, gc, image, 
		offset_x, offset_y, 0, 0, win_w - offset_x, win_h - offset_y, False);
}

void x11_sync()
{
	XSync(display, False);
}

