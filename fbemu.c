#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <unistd.h>

#include <linux/fb.h>
#include <linux/matroxfb.h>

#include "preload.h"
#include "fbemu.h"
#include "protocol.h"
#include "client.h"
#include "log.h"

static int servfd = -1;

static int fb_close(int fd);

static int fb_ioctl(int fd, unsigned long request, void * argp);

static void *fb_mmap(void *addr, size_t length, int prot, int flags,
	int fd, off_t offset);

int fb_open(const char *pathname, int flags, mode_t mode, FileOps *ops)
{
	if (servfd == -1) {
		servfd = client_connect();
		if (servfd == -1) {
			return -1;
		}
	}

	int fd = real_open("/dev/zero", O_RDWR, 0666);
	if (fd == -1) return -1;

	TRACE("Opened fb device %s", pathname);

	ops->close = fb_close;
	ops->ioctl = fb_ioctl;
	ops->mmap = fb_mmap;

	return fd;
};

int fb_close(int fd)
{
	return real_close(fd);
};

static int fb_get_vscreeninfo(struct fb_var_screeninfo * info)
{
	memset(info, 0, sizeof(struct fb_var_screeninfo));

	XFPPacket pkt;
	pkt.type = XFP_TYPE_GET_SIZE;
	send_packet(servfd, &pkt);
	if (!CHECKED_RECV_PACKET(servfd, &pkt, XFP_TYPE_GET_SIZE)) {
		WARNING("Failed to receive size packet");
		return -1;
	}

	info->xres = pkt.data.size.w;
	info->yres = pkt.data.size.h;
	info->xres_virtual = pkt.data.size.w;
	info->yres_virtual = pkt.data.size.h;
	info->xoffset = 0;
	info->yoffset = 0;
	info->bits_per_pixel = pkt.data.size.depth;
	info->grayscale = 0;

	/* FIXME: Completely bogus. */
	info->red.offset = 0;
	info->red.length = 5;
	info->green.offset = 5;
	info->green.length = 6;
	info->blue.offset = 11;
	info->blue.length = 5;

	info->nonstd = 0;
	info->height = 50;
	info->width = 70;

	info->sync = FB_SYNC_EXT;
	info->vmode = FB_VMODE_NONINTERLACED;
	info->rotate = FB_ROTATE_UR;
	
	return 0;
}

static int fb_put_vscreeninfo(struct fb_var_screeninfo * info)
{
	LOG("FB req res: %ux%u; bpp: %u",
		info->xres, info->yres, info->bits_per_pixel);

#if 0
	XFPPacket pkt;
	pkt.type = XFP_TYPE_OFFSET;
	pkt.data.offset.x = info->xoffset;
	pkt.data.offset.y = info->yoffset;
	send_packet(&pkt);
#endif

	return 0;
}

static int fb_get_fscreeninfo(struct fb_fix_screeninfo * info)
{
	memset(info, 0, sizeof(struct fb_fix_screeninfo));

	XFPPacket pkt;
	pkt.type = XFP_TYPE_GET_SIZE;
	send_packet(servfd, &pkt);
	if (!CHECKED_RECV_PACKET(servfd, &pkt, XFP_TYPE_GET_SIZE)) {
		WARNING("Failed to receive size packet");
		return -1;
	}

	strcpy(info->id, "X11FB");
	info->line_length = pkt.data.size.w * (pkt.data.size.depth / 8);
	info->type = FB_TYPE_PACKED_PIXELS;
	info->visual = FB_VISUAL_TRUECOLOR;
	info->accel = FB_ACCEL_NONE;

	info->smem_len = pkt.data.size.shm;
	info->mmio_len = pkt.data.size.shm;

	return 0;
}

static int fb_pan_display(struct fb_var_screeninfo * info)
{
	return 0;
}

static int fb_wait_for_vsync()
{
	XFPPacket pkt;
	pkt.type = XFP_TYPE_WAIT_SYNC;
	send_packet(servfd, &pkt);
	if (CHECKED_RECV_PACKET(servfd, &pkt, XFP_TYPE_WAIT_SYNC)) {
		/* Above call blocks. */
		return 0;
	}
	WARNING("Couldn't sync");
	return -1;
}

int fb_ioctl(int fd, unsigned long request, void * argp)
{
	TRACE("FB ioctl 0x%lx", request);
	switch (request) {
		case FBIOGET_VSCREENINFO: /* 0x4600 */
			return fb_get_vscreeninfo(argp);
		break;
		case FBIOPUT_VSCREENINFO: /* 0x4601 */
			return fb_put_vscreeninfo(argp);
		break;
		case FBIOGET_FSCREENINFO: /* 0x4602 */
			return fb_get_fscreeninfo(argp);
		break;
		case FBIOPAN_DISPLAY:     /* 0x4606 */
			return fb_pan_display(argp);
		break;
		case FBIO_WAITFORVSYNC:
			return fb_wait_for_vsync();
		break;
		default:
			LOG("Unknown fbdev ioctl 0x%lx", request);
		break;
	}
	return 0;
};

void *fb_mmap(void *addr, size_t length, int prot, int flags,
	int fd, off_t offset)
{
	void * res = (void*)(-1);
	XFPPacket pkt;

	TRACE("fb_mmap called p=%p, l=%zu, prot=%d, flags=%d",
		addr, length, prot, flags);

	pkt.type = XFP_TYPE_GET_SHM;
	send_packet(servfd, &pkt);
	if (!CHECKED_RECV_PACKET(servfd, &pkt, XFP_TYPE_GET_SHM)) {
		WARNING("Failed to receive data");
		return res;
	}

	res = shmat(pkt.data.shm.shmid, addr, 0);

	return res;
};

