#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <stdint.h>

#include "config.h"

#define FB_SOCKET_FILE "/tmp/x11fb-sock"

enum {
	/* Ping */
	XFP_TYPE_HELLO = 0,
	/* Asks for the shared memory segment from server */
	XFP_TYPE_GET_SHM,
	/* Asks for the current window size. */
	XFP_TYPE_GET_SIZE,
	/* Sets a new window size. */
	XFP_TYPE_SET_SIZE,
	/* Sets new voffset in server. */
	XFP_TYPE_SET_OFFSET,
	/* Asks for server to sync */
	XFP_TYPE_WAIT_SYNC,
	/* Asks for server to wait until next mouse event */
	XFP_TYPE_WAIT_MOTION,
	/* Asks for server to wait until next key event */
	XFP_TYPE_WAIT_KEY,
	XFP_TYPE_LAST
};

typedef struct XFPPacket {
	uint32_t type;
	union {
		struct XFPHello {
			uint16_t vers;
			uint16_t ping;
		} hello;
		struct XFPSHM {
			int32_t shmid;
		} shm;
		struct XFPSize {
			uint32_t shm;
			uint16_t w;
			uint16_t h;
			uint8_t depth;
		} size;
		struct XFPOffset {
			uint16_t x;
			uint16_t y;
		} offset;
		struct XFPMotion {
			uint16_t x;
			uint16_t y;
			uint8_t gesture;
			uint8_t pressed;
		} motion;
		struct XFPKey {
			uint16_t code;
			uint8_t pressed;
		} key;
	} data;
} XFPPacket;

#endif
