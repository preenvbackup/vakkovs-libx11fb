CFLAGS?=-Os -g -Wall
LDFLAGS?=-Wl,--as-needed -Wl,-z,defs

X11FBD_CFLAGS:=-D_GNU_SOURCE $(shell pkg-config --cflags glib-2.0 x11 xext)
X11FBD_LIBS:=$(shell pkg-config --libs glib-2.0 x11 xext)
LIBX11FB_CFLAGS:=-D_GNU_SOURCE
LIBX11FB_LIBS:=-ldl -lrt

LIB_OBJ:=preload.o client.o fbemu.o touchemu.o keybemu.o miscemu.o
D_OBJ:=x11fbd.o fbserv.o x11.o

all: libx11fb.so x11fbd

libx11fb.so: $(LIB_OBJ)
	$(CC) $(LDFLAGS) -shared -o $@ $^ $(LIBX11FB_LIBS)

$(LIB_OBJ): %.o: %.c
	$(CC) -fPIC -DPIC $(LIBX11FB_CFLAGS) $(CFLAGS) -o $@ -c $<

x11fbd: $(D_OBJ)
	$(CC) $(LDFLAGS) -o $@ $^ $(X11FBD_LIBS)

$(D_OBJ): %.o: %.c
	$(CC) $(X11FBD_CFLAGS) $(CFLAGS) -o $@ -c $<

install: all
	echo TODO

clean:
	rm -f libx11fb.so x11fbd *.o

.PHONY: all install clean

