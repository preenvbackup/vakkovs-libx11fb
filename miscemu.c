#include <stdio.h>
#include <string.h>
#include <fcntl.h>

#include "config.h"
#include "preload.h"
#include "miscemu.h"
#include "log.h"

int nduid_open(const char *pathname, int flags, mode_t mode, FileOps* ops)
{
	int fd = real_open("/nduid", flags & ~O_WRONLY, 0666);
	if (fd == -1) {
		WARNING("Unable to open /nduid");
		return -1;
	}

	TRACE("Opened nduid device %s (fake fd %d)", pathname, fd);

	return fd;
}

