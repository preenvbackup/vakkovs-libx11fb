#ifndef CLIENT_H
#define CLIENT_H

#include "protocol.h"

#pragma GCC visibility push(hidden)

int client_connect();
void send_packet(int servfd, const XFPPacket *pkt);
int recv_packet(int servfd, XFPPacket *pkt);

#pragma GCC visibility pop

#define CHECKED_RECV_PACKET(servfd, pkt, ck_type) \
	(recv_packet((servfd), (pkt)) && (pkt)->type == (ck_type))

#endif
