#ifndef KEYBEMU_H
#define KEYBEMU_H

#define KEYB_PATH "/dev/input/keypad"

#include "preload.h"

#pragma GCC visibility push(hidden)

extern int keyb_open(const char *pathname, int flags, mode_t mode, FileOps* ops);

#pragma GCC visibility pop

#endif
