#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/time.h>

#include <linux/input.h>

#include "preload.h"
#include "keybemu.h"
#include "client.h"
#include "log.h"

static int servfd = -1;

static int keyb_close(int fd);
static int keyb_ioctl(int fd, unsigned long request, void * argp);
static ssize_t keyb_read(int fd, void *buf, size_t count);

int keyb_open(const char *pathname, int flags, mode_t mode, FileOps* ops)
{
	if (servfd == -1) {
		servfd = client_connect();
		if (servfd == -1) {
			return -1;
		}
	}

	int fd = real_open("/dev/zero", O_RDONLY, 0666);
	if (fd == -1) return -1;

	TRACE("Opened keyb device %s", pathname);

	ops->close = keyb_close;
	ops->ioctl = keyb_ioctl;
	ops->read = keyb_read;

	return fd;
}

static int keyb_close(int fd)
{
	return real_close(fd);
}

static int keyb_ioctl(int fd, unsigned long request, void * argp)
{
	LOG("Keyb got an ioctl! 0x%lx", request);
	return 0;
}

static ssize_t keyb_read(int fd, void *buf, size_t count)
{
	XFPPacket pkt;

	pkt.type = XFP_TYPE_WAIT_KEY;
	send_packet(servfd, &pkt);

	if (CHECKED_RECV_PACKET(servfd, &pkt, XFP_TYPE_WAIT_KEY)) { /* Blocks */
		struct timeval tp;
		struct input_event e[2];
		size_t len;
		int i = 0;

		gettimeofday(&tp, NULL);

		e[i].time = tp;
		e[i].type = EV_KEY;
		e[i].code = pkt.data.key.code;
		e[i].value = pkt.data.key.pressed;
		i++;

		e[i].time = tp;
		e[i].type = EV_SYN;
		e[i].code = SYN_REPORT;
		e[i].value = 0;
		i++;

		len = sizeof(struct input_event) * i;
		if (count < len) len = count; /* Truncate */
		memcpy(buf, e, len);
		return len;
	}

	return -1;
}

