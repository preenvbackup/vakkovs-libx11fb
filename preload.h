#ifndef PRELOAD_H
#define PRELOAD_H

#include <sys/types.h>
#include <dlfcn.h>

#define STRINGIFY(s) str(s)
#define str(s) #s

#define INIT_OVERRIDE(func) \
	do { \
		if (! (real_ ## func)) { \
			dlerror(); \
			real_ ## func = (func ## _type) dlsym(RTLD_NEXT, STRINGIFY(func)); \
			if ((dl_error = dlerror()) != NULL) { \
				fprintf(stderr, "LINKER: %s\n", dl_error); \
				exit(EXIT_FAILURE); \
			} \
		} \
	} while (0);

typedef int (*open_type)(const char *pathname, int flags, ...);
typedef int (*close_type)(int fd);
typedef int (*ioctl_type)(int fd, unsigned long request, ...);
typedef ssize_t (*read_type)(int fd, void *buf, size_t count);
typedef ssize_t (*write_type)(int fd, const void *buf, size_t count);
typedef void* (*mmap_type)(void *addr, size_t length, int prot, int flags,
                           int fd, off_t offset);
typedef int (*munmap_type)(void *addr, size_t length);


typedef int (*ioctl_op_type)(int fd, unsigned long request, void*argP);


typedef struct {
	void * priv;
	close_type close;
	ioctl_op_type ioctl;
	read_type read;
	write_type write;
	mmap_type mmap;
	munmap_type munmap;
} FileOps;


#pragma GCC visibility push(hidden)

extern open_type real_open;
extern close_type real_close;
extern ioctl_type real_ioctl;
extern read_type real_read;
extern write_type real_write;
extern mmap_type real_mmap;
extern munmap_type real_munmap;

#pragma GCC visibility pop


#endif
