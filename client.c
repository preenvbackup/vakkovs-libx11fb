#include <sys/socket.h>
#include <sys/un.h>

#include "preload.h"
#include "client.h"
#include "log.h"

int client_connect()
{
	int servfd = socket(AF_UNIX, SOCK_STREAM, 0);
	XFPPacket pkt;

	if (servfd == -1) {
		perror("Failed to create socket");
		return -1;
	}
	struct sockaddr_un remote;
	remote.sun_family = AF_UNIX;
	strcpy(remote.sun_path, FB_SOCKET_FILE);
	if (connect(servfd, &remote, SUN_LEN(&remote)) < 0) {
		real_close(servfd);
		perror("Failed to bind socket");
		return -1;
	}

	pkt.type = XFP_TYPE_HELLO;
	pkt.data.hello.vers = 1;
	pkt.data.hello.ping = 42;
	send_packet(servfd, &pkt);
	if (CHECKED_RECV_PACKET(servfd, &pkt, XFP_TYPE_HELLO) &&
		pkt.data.hello.vers == 1 && pkt.data.hello.ping == 43) {
		TRACE("Ping received %u", pkt.data.hello.ping);
		return servfd;
	}

	WARNING("Ping not received");

	return servfd;
}

void send_packet(int servfd, const XFPPacket *pkt)
{
	int res = send(servfd, pkt, sizeof(XFPPacket), 0);
	if (res < 0) {
		perror("Failed to send");
	} else if (res == 0) {
		WARNING("Remote died");
	}
}

int recv_packet(int servfd, XFPPacket *pkt)
{
	int res = recv(servfd, pkt, sizeof(XFPPacket), 0);
	if (res < 0) {
		perror("Failed to recv");
		return 0;
	} else if (res == 0) {
		WARNING("Remote died");
		return 0;
	} else {
		return res == sizeof(XFPPacket);
	}
}

