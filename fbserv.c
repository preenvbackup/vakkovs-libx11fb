#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <string.h>
#include <glib.h>

#include "fbserv.h"
#include "protocol.h"
#include "x11.h"

static GIOChannel *s_serv;
static guint s_serv_watch;

typedef struct Client {
	GIOChannel *ch;
	guint watch;
} Client;
typedef struct Queue {
	XFPPacket q[MAX_QUEUED_EVENTS];
	int rp, wp;
} Queue;

static GList *clients = NULL;
static GList *motion_waitlist = NULL;
static GList *key_waitlist = NULL;

static Queue motion_queue = { { {0} }, 0, 0 };
static Queue key_queue = { { {0} }, 0, 0 };

static int queue_empty(const Queue *q)
{
	return q->wp == q->rp;
}

static int queue_full(const Queue *q)
{
	return (q->wp + 1) % MAX_QUEUED_EVENTS == q->rp;
}

static const XFPPacket* queue_pop(Queue *q)
{
	int pos = q->rp;
	g_return_val_if_fail(!queue_empty(q), NULL);
	q->rp = (q->rp + 1) % MAX_QUEUED_EVENTS;
	return &q->q[pos];
}

static void queue_push(Queue *q, const XFPPacket* pkt)
{
	int pos = q->wp;
	g_return_if_fail(!queue_full(q));
	q->wp = (q->wp + 1) % MAX_QUEUED_EVENTS;
	q->q[pos] = *pkt;
}

static void send_packet(Client *c, const XFPPacket *pkt)
{
	int res = send(g_io_channel_unix_get_fd(c->ch), pkt, sizeof(XFPPacket), 0);
	g_return_if_fail(res == sizeof(XFPPacket));
}

static void handle_packet(Client *c, const XFPPacket *pkt)
{
	XFPPacket ans;
	switch (pkt->type) {
		case XFP_TYPE_HELLO:
			g_debug("Hello received from vers %u ping %u",
				pkt->data.hello.vers, pkt->data.hello.ping);
			ans.type = XFP_TYPE_HELLO;
			ans.data.hello.vers = 1;
			ans.data.hello.ping = pkt->data.hello.ping + 1;
			send_packet(c, &ans);
		break;
		case XFP_TYPE_GET_SHM:
			g_debug("GET_SHM received");
			ans.type = XFP_TYPE_GET_SHM;
			ans.data.shm.shmid = x11_get_shmid();
			send_packet(c, &ans);
		break;
		case XFP_TYPE_GET_SIZE:
			g_debug("GET_SIZE received");
			ans.type = XFP_TYPE_GET_SIZE;
			ans.data.size.shm = shm_size;
			ans.data.size.w = win_w;
			ans.data.size.h = win_h;
			ans.data.size.depth = win_depth;
			send_packet(c, &ans);
		break;
		case XFP_TYPE_SET_SIZE:
			g_warning("SET_SIZE received");
		break;
		case XFP_TYPE_SET_OFFSET:
			x11_set_offset(ans.data.offset.x, ans.data.offset.y);
		break;
		case XFP_TYPE_WAIT_SYNC:
			x11_draw();
			x11_sync();
			ans.type = XFP_TYPE_WAIT_SYNC;
			send_packet(c, &ans);
		break;
		case XFP_TYPE_WAIT_MOTION:
			if (queue_empty(&motion_queue)) {
				/* Aha, no data: block it. */
				motion_waitlist = g_list_prepend(motion_waitlist, c);
			} else {
				send_packet(c, queue_pop(&motion_queue));
			}
		break;
		case XFP_TYPE_WAIT_KEY:
			if (queue_empty(&key_queue)) {
				key_waitlist = g_list_prepend(key_waitlist, c);
			} else {
				send_packet(c, queue_pop(&key_queue));
			}
		break;
	}
}

static void kill_client(Client *c)
{
	g_source_remove(c->watch);
	g_io_channel_unref(c->ch);

	motion_waitlist = g_list_remove(motion_waitlist, c);
	key_waitlist = g_list_remove(key_waitlist, c);
	clients = g_list_remove(clients, c);
}

static gboolean client_watch_cb(GIOChannel *source, GIOCondition condition,
	gpointer data)
{
	Client *c = (Client *) data;
	if (condition & G_IO_ERR) {
		g_warning("Error condition in x11fb client sock");
		return FALSE;
	}
	if (condition & G_IO_IN) {
		XFPPacket pkt;
		
		int res = recv(g_io_channel_unix_get_fd(c->ch), &pkt,
			sizeof(XFPPacket), 0);
		if (res == 0) {
			/* Hangup! */
			kill_client(c);
			return FALSE;
		}
		g_return_val_if_fail(res == sizeof(XFPPacket), FALSE);

		handle_packet(c, &pkt);
	}
	return TRUE;
}

static gboolean s_serv_watch_cb(GIOChannel *source, GIOCondition condition,
	gpointer data)
{
	if (condition & G_IO_ERR) {
		g_warning("Error condition in x11fb-sock");
		return FALSE;
	}
	if (condition & G_IO_IN) {
		struct sockaddr_un remote;
		socklen_t remote_len;
		int fd = accept(g_io_channel_unix_get_fd(s_serv), &remote, &remote_len);
		if (fd == -1) {
			g_warning("Failed to accept client");
			return TRUE;
		}
		g_debug("New client");
		Client *c = g_new(Client, 1);
		c->ch = g_io_channel_unix_new(fd);
		c->watch = g_io_add_watch(c->ch, G_IO_IN | G_IO_ERR,
			client_watch_cb, c);
		clients = g_list_prepend(clients, c);
	}
	return TRUE;
}

void fbserv_start()
{
	int s;
	struct sockaddr_un local;
	int len, res;

	s = socket(AF_UNIX, SOCK_STREAM, 0);
	g_return_if_fail(s != -1);

	local.sun_family = AF_UNIX;
	strcpy(local.sun_path, FB_SOCKET_FILE);
	unlink(FB_SOCKET_FILE);
	len = strlen(FB_SOCKET_FILE) + sizeof(local.sun_family);
	res = bind(s, (struct sockaddr *)&local, len);
	g_return_if_fail(res == 0);

	res = listen(s, 5);
	g_return_if_fail(res == 0);

	s_serv = g_io_channel_unix_new(s);
	s_serv_watch = g_io_add_watch(s_serv, G_IO_IN | G_IO_ERR,
		s_serv_watch_cb, NULL);
}

void fbserv_stop()
{
	g_io_channel_unref(s_serv);
	unlink(FB_SOCKET_FILE);
}

void fbserv_notify_motion(int x, int y, unsigned char pressed)
{
	XFPPacket pkt;
	pkt.type = XFP_TYPE_WAIT_MOTION;
	pkt.data.motion.x = x;
	pkt.data.motion.y = y;
	pkt.data.motion.gesture = 0;
	pkt.data.motion.pressed = pressed;

	if (motion_waitlist) {
		/* There are listeners waiting, send this packet to them */
		g_list_foreach(motion_waitlist, (GFunc) send_packet, &pkt);
		g_list_free(motion_waitlist);
		motion_waitlist = NULL;
	} else if (!queue_full(&motion_queue)) {
		/* No listeners, store the packet. */
		queue_push(&motion_queue, &pkt);

	}
}

void fbserv_notify_key(unsigned short keycode, unsigned char pressed)
{
	XFPPacket pkt;
	pkt.type = XFP_TYPE_WAIT_KEY;
	pkt.data.key.code = keycode;
	pkt.data.key.pressed = pressed;

	if (key_waitlist) {
		g_list_foreach(key_waitlist, (GFunc) send_packet, &pkt);
		g_list_free(key_waitlist);
		key_waitlist = NULL;
	} else if (!queue_full(&key_queue)) {
		/* No listeners, store the packet. */
		queue_push(&key_queue, &pkt);
	}
}

void fbserv_notify_gesture(unsigned short type)
{
	XFPPacket pkt;
	pkt.type = XFP_TYPE_WAIT_MOTION;
	pkt.data.motion.gesture = type;
	pkt.data.motion.pressed = 0;

	if (motion_waitlist) {
		/* There are listeners waiting, send this packet to them */
		g_list_foreach(motion_waitlist, (GFunc) send_packet, &pkt);
		g_list_free(motion_waitlist);
		motion_waitlist = NULL;
	} else if (!queue_full(&motion_queue)) {
		/* No listeners, store the packet. */
		queue_push(&motion_queue, &pkt);

	}
}

