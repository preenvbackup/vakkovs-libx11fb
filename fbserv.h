#ifndef FBSERV_H
#define FBSERV_H

void fbserv_start();
void fbserv_stop();

void fbserv_notify_motion(int x, int y, unsigned char pressed);
void fbserv_notify_key(unsigned short keycode, unsigned char pressed);
void fbserv_notify_gesture(unsigned short type);

#endif
