#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <dlfcn.h>

/* make sure gcc doesn't redefine open and friends as macros */
#undef open
#undef open64

#include "preload.h"

#include "fbemu.h"
#include "touchemu.h"
#include "keybemu.h"
#include "miscemu.h"

static void init_overrides() __attribute__ ((constructor));
open_type real_open;
close_type real_close;
ioctl_type real_ioctl;
read_type real_read;
write_type real_write;
mmap_type real_mmap;
munmap_type real_munmap;

/* TODO: everything here ideally has to be reentrant, mt safe */

static void* fake_real_mmap(void *addr, size_t length, int prot, int flags,
                         int fd, off_t offset)
{
	/* A hack to workaround jemalloc reentrancy "bugs" */
	/* See: https://bugzilla.mozilla.org/show_bug.cgi?id=435683 */
	return mmap64(addr, length, prot, flags, fd, offset);
}

static void init_overrides()
{
	char *dl_error;

	INIT_OVERRIDE(open);
	INIT_OVERRIDE(close);
	INIT_OVERRIDE(ioctl);
	INIT_OVERRIDE(read);
	INIT_OVERRIDE(write);
	/* HACK: mmap: read preload.h */
	real_mmap = &fake_real_mmap;
	INIT_OVERRIDE(munmap);

}

#define MAX_FDS 8

static int fds[MAX_FDS] = { [ 0 ... MAX_FDS - 1] = -1 };
static FileOps ops[MAX_FDS];

/* TODO: This is hardly what I'd call MT safe. */

static void init_ops(FileOps* ops)
{
	ops->priv = NULL;
	ops->close = real_close;
	ops->ioctl = (ioctl_op_type) real_ioctl; /* HACK: Shut va_arg warnings */
	ops->read = real_read;
	ops->write = real_write;
	ops->mmap = real_mmap;
	ops->munmap = real_munmap;
}

static int find_empty_place()
{
	int i;
	for (i = 0; i < MAX_FDS; i++) {
		if (fds[i] == -1) {
			init_ops(&ops[i]);
			return i;
		}
	}
	return -1;
}

static void free_place(int i)
{
	fds[i] = -1;
}

static int find_fd(int fd)
{
	int i;
	if (fd == -1) return -1;
	for (i = 0; i < MAX_FDS; i++) {
		if (fds[i] == fd) {
			return i;
		}
	}
	return -1;
}

int open(const char *pathname, int flags, ...)
{
    va_list args;
	mode_t mode = 0;

	if (!real_open) return -1; /* Early open() call */

	if (flags & O_CREAT) {
        va_start(args, flags);
        if (sizeof(mode_t) < sizeof(int))
            mode = (mode_t) va_arg(args, int);
        else
            mode = va_arg(args, mode_t);
        va_end(args);
	}

	if (strncmp(FB_PATH, pathname, strlen(FB_PATH)) == 0) {
		int i = find_empty_place();
		if (i == -1) return -1;
		int fd = fb_open(pathname, flags, mode, &ops[i]);
		if (fd == -1) return -1;
		fds[i] = fd;
		return fd;
	} else if (strncmp(TOUCH_PATH, pathname, strlen(TOUCH_PATH)) == 0) {
		int i = find_empty_place();
		if (i == -1) return -1;
		int fd = touch_open(pathname, flags, mode, &ops[i]);
		if (fd == -1) return -1;
		fds[i] = fd;
		return fd;
	} else if (strncmp(KEYB_PATH, pathname, strlen(KEYB_PATH)) == 0) {
		int i = find_empty_place();
		if (i == -1) return -1;
		int fd = keyb_open(pathname, flags, mode, &ops[i]);
		if (fd == -1) return -1;
		fds[i] = fd;
		return fd;
	} else if (strncmp(NDUID_PATH, pathname, strlen(NDUID_PATH)) == 0) {
		int i = find_empty_place();
		if (i == -1) return -1;
		int fd = nduid_open(pathname, flags, mode, &ops[i]);
		if (fd == -1) return -1;
		fds[i] = fd;
		return fd;
	} else {
		return real_open(pathname, flags, mode);
	}

	return 0;
}

int close(int fd)
{
	int i = find_fd(fd);
	if (i != -1) {
		int res = ops[i].close(fd);
		if (res == 0) {
			free_place(i);
			return 0;
		}
		return res;
	} else {
		return real_close(fd);
	}
}

int ioctl(int fd, unsigned long request, ...)
{
    va_list args;
    void *argp;
	int i;

    va_start(args, request);
    argp = va_arg(args, void *);
    va_end(args);

	if ((i = find_fd(fd)) != -1) {
		return ops[i].ioctl(fd, request, argp);
	} else {
		return real_ioctl(fd, request, argp);
	}
}

ssize_t read(int fd, void *buf, size_t count)
{
	int i;
	if ((i = find_fd(fd)) != -1) {
		return ops[i].read(fd, buf, count);
	} else {
		return real_read(fd, buf, count);
	}
}

ssize_t write(int fd, const void *buf, size_t count)
{
	int i;
	if ((i = find_fd(fd)) != -1) {
		return ops[i].write(fd, buf, count);
	} else {
		return real_write(fd, buf, count);
	}
}

void *mmap(void *addr, size_t length, int prot, int flags,
                  int fd, off_t offset)
{
	int i;
	if ((i = find_fd(fd)) != -1) {
		return ops[i].mmap(addr, length, prot, flags, fd, offset);
	} else {
		return fake_real_mmap(addr, length, prot, flags, fd, offset);
	}
}

