#ifndef X11_H
#define X11_H

extern int shm_size;
extern int win_w, win_h;
extern int win_depth;

void x11_start();
void x11_stop();

int x11_get_shmid();

void x11_set_size(int x, int y, int depth);
void x11_set_offset(int x, int y);

void x11_draw();

void x11_sync();

#endif
