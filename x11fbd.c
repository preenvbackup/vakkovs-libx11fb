#include <stdlib.h>

#include <glib.h>

#include "x11.h"
#include "fbserv.h"

static GMainLoop *mainloop;

static GOptionEntry entries[] = {
	{ "size", 's', 0, G_OPTION_ARG_INT, &shm_size, "Size of the framebuffer", "bytes" },
	{ "width", 'w', 0, G_OPTION_ARG_INT, &win_w, "Default width of the window", "pixels" },
	{ "height", 'h', 0, G_OPTION_ARG_INT, &win_h, "Default height of the window", "pixels" },
	{ "depth", 'd', 0, G_OPTION_ARG_INT, &win_depth, "Default color depth", "bits" },
	{ NULL }
};

int main(int argc, char ** argv)
{
	GError *error = NULL;
	GOptionContext *context = g_option_context_new(" - x11fb daemon");
	mainloop = g_main_loop_new(NULL, FALSE);

	g_option_context_add_main_entries(context, entries, NULL);
	if (!g_option_context_parse(context, &argc, &argv, &error)) {
		g_printerr("Option parsing failed: %s\n", error->message);
		return 1;
	}

	x11_start();
	fbserv_start();

	g_main_loop_run(mainloop);

	fbserv_stop();
	x11_stop();

	return 0;
}

