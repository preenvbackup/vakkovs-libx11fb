#ifndef FBEMU_H
#define FBEMU_H

#define FB_PATH "/dev/fb"

#include "preload.h"

#pragma GCC visibility push(hidden)

extern int fb_open(const char *pathname, int flags, mode_t mode, FileOps* ops);

#pragma GCC visibility pop

#endif
