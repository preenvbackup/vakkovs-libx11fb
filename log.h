#ifndef LOG_H
#define LOG_H

#include <stdio.h>

#define LOG_LEVEL 1

#define WARNING(msg, ...) fprintf(stderr, msg "\n", ## __VA_ARGS__)

#if LOG_LEVEL >= 1
#define LOG(msg, ...) printf(msg "\n", ## __VA_ARGS__)
#else
#define LOG(msg, ...) /* msg */
#endif

#if LOG_LEVEL >= 2
#define TRACE(msg, ...) printf(msg "\n", ## __VA_ARGS__)
#else
#define TRACE(msg, ...) /* msg */
#endif

#endif
